package core;

import controller.Controller;
import model.Model;
import view.View;

/**
 * Created by rakiop on 26.05.14.
 */
public class Temporal {

    public static void main(String[] args) {
        try{
            View v = new View();
            Model m = new Model();
            Controller c = new Controller(v,m);
            c.startLogic();
        }catch(Exception e){
            // @todo
        }
    }
}
