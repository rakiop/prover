package view;

import javax.swing.*;

/**
 * Created by rakiop on 26.05.14.
 */
public class View {

    private JFrame frame;

    private MainView mainView;
    private FormulaTreeView formulaTreeView;
    private SemanticTreeView semanticTreeView;
    private NegateTreeView negateTreeView;
    private SettingsView settingsView;

    private JTabbedPane jTabbedPane;

    public View(){
        frame = new JFrame();
        frame.setTitle("Prover");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 600);
        frame.setLocationRelativeTo(null);

        initMenu();

        mainView = new MainView();
        formulaTreeView = new FormulaTreeView();
        semanticTreeView = new SemanticTreeView();
        negateTreeView = new NegateTreeView();
        settingsView = new SettingsView();

        jTabbedPane = new JTabbedPane();
        jTabbedPane.add("Dane wejściowe", mainView);
        jTabbedPane.add("Drzewo ONP", formulaTreeView);
        jTabbedPane.add("Drzewo Zanegowane", negateTreeView);
        jTabbedPane.add("Drzewo semantyczne", semanticTreeView);
        jTabbedPane.add("Ustawienia", settingsView);

        frame.getContentPane().add(jTabbedPane);

        frame.setVisible(true);
    }

    protected JMenuItem exit;

    public void initMenu(){
        JMenuBar menu = new JMenuBar();

        JMenu prover = new JMenu("Prover");
        exit = new JMenuItem("Exit");
        exit.setToolTipText("Exit application");
        prover.add(exit);

        menu.add(prover);

        frame.setJMenuBar(menu);
    }

    public JMenuItem getExitButton(){
        return exit;
    }

    public MainView getMainView() {
        return mainView;
    }

    public FormulaTreeView getFormulaTreeView() { return formulaTreeView;}

    public SemanticTreeView getSemanticTreeView() { return semanticTreeView;}

    public NegateTreeView getNegateTreeView() {
        return negateTreeView;
    }

    public SettingsView getSettingsView(){return settingsView; }
}
