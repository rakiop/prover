package view;

import javax.swing.*;
import java.awt.*;

public class SettingsView extends JComponent {

	private static final long serialVersionUID = 1L;

	JLabel settingsLabel[];
	JTextField priorityField[];
    JTextField settingsField[];
    JTextField negInput;
    JTextField andInput;
    JTextField orInput;
    JTextField impInput;
    JTextField eqInput;
    JTextField fInput;
    JTextField gInput;
    JTextField negPriority;
    JTextField andPriority;
    JTextField orPriority;
    JTextField impPriority;
    JTextField eqPriority;
    JTextField fPriority;
    JTextField gPriority;

    JCheckBox stepMode;
    JCheckBox notNeg;
    JTextField stepSize;

    JCheckBox cheatMode;

    JButton saveButton;
    JButton loadFile;
    JButton saveFile;


	public SettingsView(){
		
		this.setLayout(new GridBagLayout());
	    GridBagConstraints c = new GridBagConstraints();
	    
	    settingsLabel = new JLabel[10];
	    settingsField= new JTextField[10];
	    priorityField = new JTextField[10];
		settingsLabel[0] = new JLabel("Symbole operatorów:    ");
		
		for(int i=1; i<=7; i++)
		{
			settingsField[i] = new JTextField(10);
			priorityField[i] = new JTextField(5);
		}
		
		settingsLabel[1] = new JLabel("Negacja:");
		negInput = settingsField[1]; 
		settingsLabel[2] = new JLabel("Koniunkcja:");
		andInput = settingsField[2];
		settingsLabel[3] = new JLabel("Alternatywa:");
		orInput = settingsField[3];
		settingsLabel[4] = new JLabel("Implikacja:");
		 impInput = settingsField[4];
		settingsLabel[5] = new JLabel("Równoważność:");
		 eqInput = settingsField[5];
		settingsLabel[6] = new JLabel("Zawsze:");
		 fInput = settingsField[6];
		settingsLabel[7] = new JLabel("Kiedyś:");
		gInput = settingsField[7];
		
		negPriority = priorityField[1]; 
		andPriority = priorityField[2];
		orPriority = priorityField[3];
		 impPriority = priorityField[4];
		 eqPriority = priorityField[5];
		 fPriority = priorityField[6];
		gPriority = priorityField[7];
		
		saveButton = new JButton("Zatwierdź ustawienia");
		loadFile = new JButton("Wczytaj ustawienia z pliku");
		saveFile = new JButton("Zapisz ustawienia do pliku");
		c.gridx=1;
		c.gridy=0;
		this.add(new JLabel("Symbole operatorów:"), c);
		c.gridx=2;
		c.gridy=0;
		this.add(new JLabel("Priorytet:"));
		
		for(int i=1; i<=7; i++)
		{
			c.gridx=0;
			c.gridy=i;
			this.add(settingsLabel[i], c);
			c.gridx=1;
			this.add(settingsField[i], c);
			c.gridx=2;
			this.add(priorityField[i], c);
		}
		
		for(int i=8; i<10; i++)
		{
			c.gridx=1;
			c.gridy=i;
			this.add(new JLabel(" "), c);
		}
		
		
		c.gridx=1;
		c.gridy=13;
		this.add(new JLabel("Tryb pracy krokowej"),c);
		
		stepMode = new JCheckBox();
		c.gridx=2;
		this.add(stepMode,c);
		
		c.gridx=1;
		c.gridy=14;
		settingsLabel[8] = new JLabel("Skok dla pracy krokowej:");
		this.add(settingsLabel[8], c);
		
		stepSize = new JTextField(5);
        stepSize.setText("1");
		settingsField[8] = stepSize;
		
		c.gridx=2;
		this.add(settingsField[8], c);

        c.gridx = 1;
        c.gridy = 16;
        this.add(new JLabel("Wcześniejsze zamyknięcie węzła przy wykryciu dopełnionych literałów:"), c);

        c.gridx = 2;
        cheatMode = new JCheckBox();
        cheatMode.setSelected(true);
        this.add(cheatMode, c);

		c.gridy=18;
		c.gridx=2;
		notNeg = new JCheckBox();
		
		c.gridx=1;
		c.gridy=20;
		this.add(new JLabel(" "), c);
		
		c.gridx=1;
		c.gridy=22;
		this.add(saveButton, c);
		c.gridx=1;
		c.gridy=24;
		this.add(loadFile, c);
		c.gridx=1;
		c.gridy=26;
		this.add(saveFile, c);

	}

	public JTextField getNegInput() {
		return negInput;
	}

	public JTextField getAndInput() {
		return andInput;
	}

	public JTextField getOrInput() {
		return orInput;
	}

	public JTextField getImpInput() {
		return impInput;
	}

	public JTextField getEqInput() {
		return eqInput;
	}

	public JTextField getfInput() {
		return fInput;
	}

	public JTextField getgInput() {
		return gInput;
	}

	public JButton getSaveButton() {
		return saveButton;
	}
	
	public JButton getSaveFileButton() {
		return saveFile;
	}
	
	public JButton getLoadFileButton() {
		return loadFile;
	}
	
	public JTextField getNegPriority() {
		return negPriority;
	}	
	public JTextField getAndPriority() {
		return andPriority;
	}

	public JTextField getOrPriority() {
		return orPriority;
	}

	public JTextField getImpPriority() {
		return impPriority;
	}

	public JTextField getEqPriority() {
		return eqPriority;
	}

	public JTextField getfPriority() {
		return fPriority;
	}

	public JTextField getgPriority() {
		return gPriority;
	}

	public JTextField getStepSize() {
		return stepSize;
	}

	public JCheckBox getStepMode() {
		return stepMode;
	}
	
	public JCheckBox getNegMode() {
		return notNeg;
	}

	public JCheckBox getCheatMode() { return cheatMode; }
}
