package view;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.graph.Forest;
import edu.uci.ics.jung.visualization.VisualizationImageServer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ScalingControl;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.PickableVertexPaintTransformer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.picking.MultiPickedState;
import edu.uci.ics.jung.visualization.picking.PickedState;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import edu.uci.ics.jung.visualization.subLayout.TreeCollapser;
import model.parser.FormulaNode;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ConstantTransformer;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

/**
 * Created by rakiop on 26.05.14.
 */
public class NegateTreeView extends JComponent  {

        private static final long serialVersionUID = 1L;
        private DelegateTree<FormulaNode, String> g;

        public NegateTreeView() {
        }

        public void setTree(DelegateTree<FormulaNode, String> g) {
            this.g = g;
        }

        public void showTree() {
            final Layout<FormulaNode, String> l = new TreeLayout<FormulaNode, String>(g);
            final VisualizationViewer<FormulaNode, String> v;
            final TreeCollapser collapser = new TreeCollapser();

            v =  new VisualizationViewer<FormulaNode, String>(l, new Dimension(600,400));
            final PickedState<FormulaNode> pvs = new MultiPickedState<FormulaNode>();
            v.getRenderContext().setVertexFillPaintTransformer(new PickableVertexPaintTransformer<FormulaNode>(pvs, Color.WHITE, Color.yellow));
            v.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.CNTR);
            v.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<FormulaNode, String>());
            v.getRenderContext().setVertexLabelTransformer(new ToStringLabeller(){
                @Override
                public String transform(Object v) {
                    if(v instanceof FormulaNode){
                        return v.toString();
                    }
                    return "...";
                }
            });

            v.getRenderContext().setArrowFillPaintTransformer(new ConstantTransformer(Color.WHITE));
            v.getRenderContext().setVertexShapeTransformer(new Transformer(){
                public Shape transform(Object i){
                    Ellipse2D circle = new Ellipse2D.Double(-15, -15, 30, 30);
                    return AffineTransform.getScaleInstance(0.95, 0.95).createTransformedShape(circle);
                }
            });

            this.add(v);

            this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));

            final DefaultModalGraphMouse graphMouse = new DefaultModalGraphMouse();
            v.setGraphMouse(graphMouse);
            graphMouse.setMode(ModalGraphMouse.Mode.PICKING);
            this.repaint();

            JButton collapse = new JButton("Zwiń");
            JButton expand = new JButton("Rozwiń");

//            this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
            JComboBox modeBox = graphMouse.getModeComboBox();
            modeBox.addItemListener(graphMouse.getModeListener());
            graphMouse.setMode(ModalGraphMouse.Mode.PICKING);
            final ScalingControl scaler = new CrossoverScalingControl();

        /* przyblizanie */
            JButton plus = new JButton("+");
            plus.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    scaler.scale(v, 1.1f, v.getCenter());
                }
            });
        /* oddalanie */
            JButton minus = new JButton("-");
            minus.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    scaler.scale(v, 1/1.1f, v.getCenter());
                }
            });

            JPanel scaleGrid = new JPanel(new GridLayout(1,0));
            scaleGrid.setBorder(BorderFactory.createTitledBorder("Zbliżenie"));

        /* buttony zapisu do pliku i krok */
            JButton saveButton = new JButton("Zapisz do pliku");


        /* panel przyciskow kontrolnych */
            JPanel controls = new JPanel();
            controls.setBorder(new LineBorder(Color.lightGray));
            scaleGrid.add(plus);
            scaleGrid.add(minus);
            controls.add(scaleGrid);
            controls.add(modeBox);
            controls.add(collapse);
            controls.add(expand);
            controls.add(new JLabel("       "));
            controls.add(saveButton);

            this.add(controls, BorderLayout.SOUTH);

            /* panel szczegolow o wezle */
            JPanel details = new JPanel();
            details.setBorder(new LineBorder(Color.LIGHT_GRAY));
            details.setBackground(Color.WHITE);
            final JLabel detailsContent = new JLabel("Zaznacz węzeł, aby wyświetlić szczegółowe informacje");

            details.add(detailsContent);
            this.add(details, BorderLayout.SOUTH);
            this.repaint();

            final PickedState<FormulaNode> pickedState = v.getPickedVertexState();
            pickedState.addItemListener(new ItemListener() {

                public void itemStateChanged(ItemEvent e) {
                    Object subject = e.getItem();
                    if (subject instanceof FormulaNode) {
                        FormulaNode vertex = (FormulaNode) subject;
                        if (pickedState.isPicked(vertex)) {
                            detailsContent.setText(vertex.getFormulaAsString());
                        }
                    }
                }
            });

            /* zwijanie drzewa */

            collapse.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Collection<FormulaNode> picked =new HashSet<FormulaNode>(v.getPickedVertexState().getPicked());
                    if(picked.size() == 1) {
                        Object root = picked.iterator().next();
                        FormulaNode node = (FormulaNode)root;

                        Forest<FormulaNode, String> inGraph = (Forest<FormulaNode, String>)l.getGraph();
                        try {
                            if(!node.isRoot())
                                collapser.collapse(v.getGraphLayout(), inGraph, root);
                            else
                                detailsContent.setText("Nie można zwinąć głównego węzła !");
                        } catch (InstantiationException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        } catch (IllegalAccessException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                        v.getPickedVertexState().clear();
                        v.repaint();
                    }
                }
            });

		/* rozwijanie drzewa */
            expand.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    Collection picked = v.getPickedVertexState().getPicked();
                    for(Object vv : picked) {
                        if(vv instanceof Forest) {
                            Forest<FormulaNode, String> inGraph = (Forest<FormulaNode, String>)l.getGraph();
                            collapser.expand(inGraph, (Forest<FormulaNode, String>)vv);
                        }
                        v.getPickedVertexState().clear();
                        v.repaint();
                    }
                }});

            final NegateTreeView comp = this;

            saveButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    File fileName = null;

                    JFileChooser chooser = new JFileChooser();
                    chooser.setCurrentDirectory( new File( "./") );
                    int actionDialog = chooser.showSaveDialog(comp);
                    if ( actionDialog == JFileChooser.APPROVE_OPTION )
                    {
                        fileName = new File( chooser.getSelectedFile( ) + ".png" );
                        if(fileName == null)
                            return;
                        if(fileName.exists())
                        {
                            actionDialog = JOptionPane.showConfirmDialog(comp,
                                    "Replace existing file?");
                            // may need to check for cancel option as well
                            if (actionDialog == JOptionPane.NO_OPTION)
                                return;
                        }
                    }

                    VisualizationImageServer<FormulaNode, String> vis =
                            new VisualizationImageServer<FormulaNode, String>(v.getGraphLayout(),
                                    v.getGraphLayout().getSize());
                    vis.getRenderContext().setVertexFillPaintTransformer(new PickableVertexPaintTransformer<FormulaNode>(pvs, Color.lightGray, Color.yellow));

                    vis.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.CNTR); //j

                    vis.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<FormulaNode, String>());
                    vis.getRenderContext().setVertexLabelTransformer(new ToStringLabeller(){
                        @Override

                        public String transform(Object v) {

                            if(v instanceof FormulaNode){
                                return v.toString();
                            }

                            return "...";
                        }
                    });
                    vis.getRenderContext().setArrowFillPaintTransformer(new ConstantTransformer(Color.lightGray));
                    vis.getRenderContext().setVertexShapeTransformer(new Transformer(){
                        public Shape transform(Object i){
                            Ellipse2D circle = new Ellipse2D.Double(-15, -15, 30, 30);
                            return AffineTransform.getScaleInstance(0.95, 0.95).createTransformedShape(circle);
                        }
                    });

                    BufferedImage image = (BufferedImage) vis.getImage(
                            new Point2D.Double(v.getGraphLayout().getSize().getWidth() / 2,
                                    v.getGraphLayout().getSize().getHeight() / 2),
                            new Dimension(v.getGraphLayout().getSize()));
                    try {
                        ImageIO.write(image, "png", fileName);
                    } catch (IOException ex) {
                        //
                    }

                }

            });

        }


    public void removeAll(){
        super.removeAll();
        this.g = null;
    }
}
