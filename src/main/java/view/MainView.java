package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by rakiop on 26.05.14.
 */
public class MainView extends JPanel {

    private static final long serialVersionUID = 1L;

    private JLabel formulaLabel;
    private JLabel formulaResultLabel;
    private JTextField formula;
    private JScrollPane formulaPane;
    private JButton proverAction;


    public MainView(){
        initWindow();
    }

    public void initWindow(){
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        this.setBackground(this.getBackground());
        //this.setLayout(null);
        formulaLabel = new JLabel("Wprowadzona formuła:");
        formulaLabel.setLocation(400, 10);
        formulaLabel.setSize(formulaLabel.getPreferredSize());
        this.add(formulaLabel);

        formula = new JTextField("(p&(p=>q))=>q");
        formula.setSize(new Dimension(800, 50));
        formulaPane = new JScrollPane();
        formula.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
        formulaPane.getViewport().add(formula);
        formulaPane.setSize(new Dimension(800, 50));
        formulaPane.setLocation(10, 30);
        this.add(formulaPane);

        proverAction = new JButton("Analizuj");
        proverAction.setSize(proverAction.getPreferredSize());
        proverAction.setLocation(870, 40);
        this.add(proverAction);

        formulaResultLabel = new JLabel("");
        formulaResultLabel.setSize(500, 80);
        formulaResultLabel.setLocation(100, 50);
        this.add(formulaResultLabel);

        JButton loadButton = new JButton("Wczytaj formułę z pliku tekstowego");
        //loadButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        loadButton.setSize(loadButton.getPreferredSize());
        loadButton.setLocation(100, 150);
        this.add(loadButton);

        final MainView x = this;

        /* wczytywanie formuly z pliku tekstowego */
        loadButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                int showOpenDialog = fileChooser.showOpenDialog(x);

                File f = fileChooser.getSelectedFile();
                BufferedReader reader = null;
                String line;
                try {
                    reader = new BufferedReader(new FileReader(f));
                    line = reader.readLine();
                    formula.setText(line);
                } catch(IOException ex) {
                    System.out.println("Nie można otworzyć pliku");
                } finally {
                    try {
                        reader.close();
                    } catch(IOException ex) {
                        System.out.println("Nie można zamknąć pliku");
                    }
                }
            }
        });

        this.setSize(1000, 200);
        this.repaint();
    }

    public JLabel getFormulaResultLabel(){
        return formulaResultLabel;
    }

    public JButton getProverAction(){
        return proverAction;
    }

    public JTextField getFormula(){
        return formula;
    }

}
