package model.parser;

import model.prover.Operator;
import model.prover.OperatorManagement;

import java.util.Stack;

/**
 * Created by rakiop on 26.05.14.
 */
public class Parser {

    private OperatorManagement operatorManagement = OperatorManagement.getInstance();
    private Stack<Operator> operatorStacks = new Stack<Operator>();
    private Stack<FormulaNode> formulaNodeStack = new Stack<FormulaNode>();
    private FormulaNode root;

    public FormulaNode parse(String formula) throws Exception{
        Operator operator;
        while(!formula.isEmpty()){
            formula = formula.trim();

            operator = operatorManagement.getToken(formula);

            String op = formula.substring(0, operatorManagement.getSymbol(operator).length());

            formula = formula.substring(operatorManagement.getSymbol(operator).length());

            if(operator == Operator.Literal){

                while(operatorManagement.getToken(formula) == Operator.Literal && formula.isEmpty() == false){
                    op += formula.substring(0, operatorManagement.getSymbol(operator).length());

                    formula = formula.substring(operatorManagement.getSymbol(operator).length());
                }

                FormulaNode formulaNode = new FormulaNode();
                formulaNode.setType(Operator.Literal);
                formulaNode.setName(op);
                formulaNode.setLeft(null);
                formulaNode.setRight(null);
                formulaNodeStack.push(formulaNode);
            }else if(operator == Operator.LeftBrace){
                operatorStacks.push(operator);
            }else if(operator == Operator.RightBrace){
                if(operatorStacks.isEmpty() == true || operatorStacks.peek() == Operator.LeftBrace){
                    throw new Exception("Błąd parsera - Błędna ilość nawiasów");
                }

                while( !operatorStacks.isEmpty() && operatorStacks.peek() != Operator.LeftBrace){
                    addNode(operatorStacks.pop());
                }
                if(operatorStacks.isEmpty() == true){
                    throw new Exception("Błąd parsera - Błędna ilość nawiasów");
                }
                operatorStacks.pop();

            }else if(operator == Operator.Unknown){
                throw new Exception("Błąd parsera - nieznany operator");
            }
            else{
                int p = operatorManagement.getPriority(operator);

                while(!operatorStacks.isEmpty()){
                    if(p >= operatorManagement.getPriority(operatorStacks.peek())){
                        break;
                    }
                    addNode(operatorStacks.pop());
                }

                operatorStacks.push(operator);
            }
        }

        while(!operatorStacks.isEmpty()){
            addNode(operatorStacks.pop());
        }
        root = formulaNodeStack.peek();
        return root;
    }

    private void addNode(Operator operator) throws Exception{
        FormulaNode fn1,fn2;

        if(formulaNodeStack.isEmpty() == true){
            throw new Exception("Błąd parsera");
        }

        fn2 = formulaNodeStack.pop();

        if( operator == Operator.NOT || operator == Operator.F || operator == Operator.G ){
            formulaNodeStack.push(new FormulaNode(operator,fn2));
        }else{
            if(formulaNodeStack.isEmpty() == true){
                throw new Exception("Bład parsera - nie znaleziono prawego argumentu operatora");
            }

            fn1 = formulaNodeStack.pop();

            formulaNodeStack.push(new FormulaNode(operator, fn1, fn2));
        }
    }
}
