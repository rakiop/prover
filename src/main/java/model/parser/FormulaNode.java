package model.parser;

import model.prover.Operator;

/**
 * Created by rakiop on 26.05.14.
 */
public class FormulaNode {

    private static int no = 1;
    private FormulaNode left;
    private FormulaNode right;
    private Operator type;
    private boolean isRoot = false;

    private String name;
    private int nr;
    private boolean negated = false;

    public FormulaNode(){
        if(no > 100000000){
            throw new  IllegalStateException("Za duże zagnieżdżenie :(");
        }
        this.nr = no;
        no++;
    }

    public FormulaNode(Operator o, FormulaNode n){
        this.type = o;
        this.left = n;
        this.right = null;
        this.nr = no;
        no++;
    }

    public FormulaNode(Operator o,FormulaNode l,FormulaNode r){
        this.type = o;
        this.right = r;
        this.left = l;
        this.nr = no;
        no++;
    }

    public static FormulaNode copy(FormulaNode node){
        FormulaNode formulaNode = new FormulaNode();

        formulaNode.name = node.name;
        formulaNode.negated = node.negated;
        formulaNode.nr = no++;
        formulaNode.type = node.type;

        if(node.left != null){
            formulaNode.left = node.left;
        }else {
            formulaNode.left = null;
        }

        if(node.right != null){
            formulaNode.right = node.right;
        }else {
            formulaNode.right = null;
        }

        return formulaNode;
    }

    public void setLeft(FormulaNode w){
        this.left = w;
    }

    public void setRight(FormulaNode w){
        this.right = w;
    }

    public void setType(Operator o){
        this.type = o;
    }

    public FormulaNode getLeft(){
        return this.left;
    }

    public FormulaNode getRight(){
        return this.right;
    }

    public Operator getType(){
        return this.type;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String s){
        this.name = s;
    }

    public boolean isNegated() {
        return negated;
    }

    public void setNegated(boolean negated) {
        this.negated = negated;
    }

    public String toString(){
        if(this.type != Operator.Literal){
            return this.type.toString();
        }else{
            if(this.negated == true){
                return "~"+this.name;
            }
            return this.name;
        }
    }

    public int getNr(){
        return this.nr;
    }

    public String getFormulaAsString(){
        switch(this.type){
            case AND:
                return String.format("%s(%s) & (%s)", (this.negated == true ? "~~" : ""), this.left.getFormulaAsString(),this.right.getFormulaAsString());

            case EQ:
                return String.format("%s(%s) <=> (%s)", (this.negated == true ? "~~" : ""), this.left.getFormulaAsString(),this.right.getFormulaAsString());

            case F:
                return String.format("F(%s)", this.left.getFormulaAsString());

            case G:
                return String.format("G(%s)", this.left.getFormulaAsString());

            case IMP:
                return String.format("%s(%s) => (%s)", (this.negated == true ? "~~" : ""), this.left.getFormulaAsString(),this.right.getFormulaAsString());

            case Literal:
                return (this.negated == true ? "~" + this.name : this.name);

            case NOT:
                return String.format("~(%s)", this.left.getFormulaAsString());

            case OR:
                return String.format("%s(%s) | (%s)", (this.negated == true ? "~~" : ""), this.left.getFormulaAsString(),this.right.getFormulaAsString());

            default:
                return "";

        }
    }

    public boolean isRoot() {
        return isRoot;
    }

    public void setRoot(boolean isRoot) {
        this.isRoot = isRoot;
    }


}
