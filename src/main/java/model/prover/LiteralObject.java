package model.prover;

/**
 * Created by rakiop on 26.05.14.
 */
public class LiteralObject {

    private String name;
    private boolean isNegated;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public boolean isNegated() {
        return isNegated;
    }
    public void setNegated(boolean negated) {
        this.isNegated = negated;
    }

    @Override
    public String toString() {
        return ( this.isNegated == true ? "~" : "" )+this.name;
    }
}
