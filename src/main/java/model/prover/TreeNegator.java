package model.prover;

import model.parser.FormulaNode;

/**
 * Created by rakiop on 26.05.14.
 */
public class TreeNegator {

    FormulaNode root;

    public FormulaNode negate(FormulaNode formula) {
        this.root = formula;

        // Sprawdzamy czy może już negujemy początek
        if (root.getType() == Operator.NOT) {
            //Wyciągamy co jest pod negowaniem
            root = root.getLeft();

            //Usuwamy negowanie
            removeNeg(root);

            return root;
        }

        negateNode(root);

        removeNeg(root);

        return root;
    }

    private void negateNode(FormulaNode node) {
        switch (node.getType()) {
            case NOT:
                // ~(~a) = a
                FormulaNode tmp = node.getLeft();
                node.setType(tmp.getType());
                node.setName(tmp.getName());
                node.setNegated(tmp.isNegated());
                node.setLeft(tmp.getLeft());
                node.setRight(tmp.getRight());
            break;

            case AND:
                // ~(a&b) = ~a|~b
                node.setType(Operator.OR);
                negateNode(node.getLeft());
                negateNode(node.getRight());
            break;

            case OR:
                // ~(a|b) = ~a&~b
                node.setType(Operator.AND);
                negateNode(node.getLeft());
                negateNode(node.getRight());
            break;

            case IMP:
                // ~(a => b) = a & ~b
                node.setType(Operator.AND);
                negateNode(node.getRight());
            break;

            case EQ:
                // ~(a <=> b) = (a&~b)|(b&~a)
                node.setType(Operator.OR);

                FormulaNode leftImpl = new FormulaNode();
                FormulaNode rightImpl = new FormulaNode();

                leftImpl.setType(Operator.IMP);
                rightImpl.setType(Operator.IMP);

                leftImpl.setLeft(FormulaNode.copy(node.getLeft()));
                leftImpl.setRight(FormulaNode.copy(node.getRight()));

                rightImpl.setLeft(FormulaNode.copy(node.getRight()));
                rightImpl.setRight(FormulaNode.copy(node.getLeft()));

                FormulaNode leftNeg = new FormulaNode();
                FormulaNode rightNeg = new FormulaNode();

                leftNeg.setType(Operator.NOT);
                rightNeg.setType(Operator.NOT);

                node.setLeft(leftNeg);
                node.setRight(rightNeg);

                leftNeg.setLeft(leftImpl);
                rightNeg.setLeft(rightImpl);
            break;

            case Literal:
                if (node.isNegated()) {
                    node.setNegated(false);
                } else {
                    node.setNegated(true);
                }
            break;

            case F:
                negateNode(node.getLeft());
            break;

            case G:
                negateNode(node.getLeft());
            break;

            default:

            break;
        }
    }

    private boolean checkIfNegated(FormulaNode node) {
        if (node == null) {
            return false;
        }

        return node.getType() == Operator.NOT;
    }

    private void removeNeg(FormulaNode node) {
        if (node.getType() == Operator.NOT) {
            FormulaNode tmp = node.getLeft();
            node.setType(tmp.getType());
            node.setName(tmp.getName());
            node.setNegated(tmp.isNegated());
            node.setLeft(tmp.getLeft());
            node.setRight(tmp.getRight());

            negateNode(node);
        }

        if (node.getLeft() != null) {
            removeNeg(node.getLeft());
        }

        if (node.getRight() != null) {
            removeNeg(node.getRight());
        }
    }

}
