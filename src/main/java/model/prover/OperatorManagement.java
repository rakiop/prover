package model.prover;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;

/**
 * Created by rakiop on 26.05.14.
 */
public class OperatorManagement {

    private HashMap<Operator,String> symbol;
    private HashMap<Operator,Integer> priority;

    private static OperatorManagement instance;

    public static OperatorManagement getInstance(){
        if(instance== null)
            instance = new OperatorManagement();
        return instance;
    }

    private OperatorManagement(){
        this.symbol = new HashMap<Operator,String>();
        this.priority = new HashMap<Operator,Integer>();
        loadDefault();
    }

    public void setSymbol(Operator operator,String string){
        this.symbol.put(operator, string);
    }

    public void setPriority(Operator operator,Integer integer){
        this.priority.put(operator, integer);
    }

    public String getSymbol(Operator operator){
        return this.symbol.get(operator);
    }

    public Integer getPriority(Operator operator){
        return this.priority.get(operator);
    }

    public Operator getToken(String string){

        for(Operator o: Operator.values() ){

            if(o != Operator.Literal)
                if(string.startsWith(symbol.get(o))){
                    return o;
                }
        }

        if(string.isEmpty() == false){
            String lit = String.valueOf(string.charAt(0));

            if(StringUtils.isAlphanumeric(lit) == false){
                return Operator.Unknown;
            }
        }
        return Operator.Literal;

    }

    private void loadDefault(){

        symbol.put(Operator.Literal, "\'");
        priority.put(Operator.Literal, 99);

        symbol.put(Operator.Unknown, "\"");
        priority.put(Operator.Unknown, 100);

        symbol.put(Operator.AND, "&");
        priority.put(Operator.AND, 2);

        symbol.put(Operator.OR, "|");
        priority.put(Operator.OR, 2);

        symbol.put(Operator.NOT, "~");
        priority.put(Operator.NOT, 3);

        symbol.put(Operator.IMP, "=>");
        priority.put(Operator.IMP, 1);

        symbol.put(Operator.EQ, "<=>");
        priority.put(Operator.EQ, 1);

        symbol.put(Operator.F, "F");
        priority.put(Operator.F, 3);

        symbol.put(Operator.G, "G");
        priority.put(Operator.G, 3);

        symbol.put(Operator.LeftBrace, "(");
        priority.put(Operator.LeftBrace, 0);

        symbol.put(Operator.RightBrace, ")");
        priority.put(Operator.RightBrace, 0);
    }

}
