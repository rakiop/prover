package model.prover;

import model.parser.FormulaNode;

/**
 * Created by rakiop on 26.05.14.
 */
public class Prover {

    private FormulaNode formulaRoot;
    private FormulaNode negatedFormulaRoot;

    private ProverNode proverRoot;

    private TreeNegator treeNegator;

    boolean output = true;

    public Prover(){
        treeNegator = new TreeNegator();
    }

    public void prove(FormulaNode formula, boolean isCheatMode){
        this.formulaRoot = formula;

        ProverNode.no = 1;
        negatedFormulaRoot = treeNegator.negate(formulaRoot);
        proverRoot = new ProverNode();
        proverRoot.setStatement(negatedFormulaRoot);
        proverRoot.decompose(isCheatMode);

        this.checkIfIsTrue(proverRoot);
    }

    private void checkIfIsTrue(ProverNode node){
        if(node.getLeft() == null && node.getRight() == null && node.checkIfClosed() != true){
            this.output = false;
        }

        if(node.getLeft() != null){
            checkIfIsTrue(node.getLeft());
        }

        if(node.getRight() != null){
            checkIfIsTrue(node.getRight());
        }
    }

    public boolean isOutput() {
        return output;
    }

    public ProverNode getProverRoot() {
        return proverRoot;
    }

    public FormulaNode getNegatedFormulaRoot() {
        return negatedFormulaRoot;
    }

}
