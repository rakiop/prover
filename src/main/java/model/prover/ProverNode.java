package model.prover;

import model.parser.FormulaNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rakiop on 26.05.14.
 */
public class ProverNode {

    public static int no = 1;

    public static long step = 1;

    private ProverNode left;
    private ProverNode right;
    private ProverNode parent;
    private boolean isRoot = false;

    private FormulaNode statement;

    private List<FormulaNode> futureStatement;

    private String name;
    private int nr;

    private List<LiteralObject> literalObjects;

    private boolean isClosed = false;
    public static boolean endStatus = false;

    public ProverNode(){
        this.nr = no;
        no++;
    }

    private void init(){
        if(literalObjects == null)
            literalObjects = new ArrayList<LiteralObject>();
        if(futureStatement == null)
            futureStatement = new ArrayList<FormulaNode>();
    }

    public void decompose(boolean isCheatMode){
        init();

        //Wyzwalamy GC jeśli mamy mniej niż 50MB
        if(Runtime.getRuntime().freeMemory() < 1024*1024*5){
            System.gc();
        }

        switch (statement.getType()) {
            //Literał
            case Literal:
                LiteralObject literal = new LiteralObject();
                literal.setName(statement.getName());
                literal.setNegated(statement.isNegated());
                boolean isClosed = addToLiterals(literal);
                if(isClosed)
                    this.isClosed = isClosed;

                if(!isCheatMode){
                    checkIfClosed(true);
                }

                saveLiteralFuture();

                if(futureStatement.isEmpty() == false && (!isCheatMode || !isClosed)){
                    ProverNode proverNode = new ProverNode();
                    proverNode.setStatement(futureStatement.get(0));
                    proverNode.setFutureStatement(futureStatement);
                    proverNode.getFutureStatement().remove(0);
                    proverNode.setLiterals(literalObjects);
                    this.left = proverNode;
                    proverNode.decompose(isCheatMode);
                }
            break;

            // I
            case AND:
                saveLiteralFuture();
                ProverNode proverNode = new ProverNode();
                proverNode.setStatement(statement.getLeft());
                proverNode.setFutureStatement(futureStatement);
                proverNode.getFutureStatement().add(0, this.statement.getRight());
                proverNode.setLiterals(literalObjects);

                this.left = proverNode;
                this.left.decompose(isCheatMode);
            break;

            // Lub
            case OR:
                saveLiteralFuture();
                ProverNode leftNode = new ProverNode();
                this.left = leftNode;

                ProverNode rightNode = new ProverNode();
                this.right = rightNode;

                leftNode.setFutureStatement(copyStatements());
                rightNode.setFutureStatement(futureStatement);
                leftNode.setLiterals(copyLiterals());
                rightNode.setLiterals(literalObjects);
                leftNode.setStatement(statement.getLeft());
                rightNode.setStatement(statement.getRight());

                    this.left.decompose(isCheatMode);
                    this.right.decompose(isCheatMode);
            break;

            // Implikacja
            case IMP:
                saveLiteralFuture();
                ProverNode leftNode1 = new ProverNode();
                this.left = leftNode1;

                ProverNode rightNode1 = new ProverNode();
                this.right = rightNode1;

                leftNode1.setFutureStatement(copyStatements());
                rightNode1.setFutureStatement(futureStatement);

                leftNode1.setLiterals(copyLiterals());
                rightNode1.setLiterals(literalObjects);

                FormulaNode leftImpl = FormulaNode.copy(statement.getLeft());

                TreeNegator neg = new TreeNegator();
                neg.negate(leftImpl);

                leftNode1.setStatement(leftImpl);
                rightNode1.setStatement(statement.getRight());

                this.right.decompose(isCheatMode);
                this.left.decompose(isCheatMode);
                break;

            // Zawsze
            case F:
                saveLiteralFuture();
                ProverNode proverNodeF = new ProverNode();
                proverNodeF.setStatement(statement.getLeft());

                proverNodeF.setFutureStatement(futureStatement);
                proverNodeF.setLiterals(literalObjects);

                this.left = proverNodeF;
                this.left.decompose(isCheatMode);
                break;

            // Kiedyś
            case G:
                saveLiteralFuture();
                ProverNode proverNodeG = new ProverNode();
                proverNodeG.setStatement(statement.getLeft());

                proverNodeG.setFutureStatement(futureStatement);
                proverNodeG.setLiterals(literalObjects);

                this.left = proverNodeG;
                this.left.decompose(isCheatMode);
            break;

            default:
            break;
        }
    }


    private String sLiteral;
    private String sFuture;

    private void saveLiteralFuture(){
        init();
        sLiteral = "";
        for(LiteralObject s : literalObjects){
            sLiteral += s.toString()+"\n";
        }

        sFuture = "";
        for(FormulaNode f : futureStatement){
            sFuture += f.getFormulaAsString() + "\n";
        }
    }

    public String getLiteral(){
        if(sLiteral == null)
            saveLiteralFuture();
        return sLiteral;
    }

    public String getFuture(){
        if(sFuture == null)
            saveLiteralFuture();
        return sFuture;
    }

    private List<LiteralObject> copyLiterals(){
        init();
        List<LiteralObject> toReturn;
        try{
            toReturn = new ArrayList<LiteralObject>(this.literalObjects.size());
        }catch(Exception e){
            e = null;
            System.gc();
            toReturn = new ArrayList<LiteralObject>(this.literalObjects.size());
        }
        for(LiteralObject literalObject: this.literalObjects){
            toReturn.add(literalObject);
        }
        return toReturn;
    }

    private List<FormulaNode> copyStatements(){
        init();
        try{
        List<FormulaNode> toReturn = new ArrayList<FormulaNode>(this.futureStatement.size());
        for(FormulaNode f: this.futureStatement){
            FormulaNode formula = f;
            toReturn.add(formula);
        }

        return toReturn;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public List<FormulaNode> getFutureStatement() {
        init();
        return futureStatement;
    }

    public void setFutureStatement(List<FormulaNode> futureStatement) {
        this.futureStatement = futureStatement;
    }

    public ProverNode getLeft() {
        return left;
    }

    public void setLeft(ProverNode left) {
        this.left = left;
    }

    public ProverNode getRight() {
        return right;
    }

    public void setRight(ProverNode right) {
        this.right = right;
    }

    public ProverNode getParent() {
        return parent;
    }

    public void setParent(ProverNode parent) {
        this.parent = parent;
    }

    public FormulaNode getStatement() {
        return statement;
    }

    public void setStatement(FormulaNode statement) {
        this.statement = statement;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public List<LiteralObject> getLiterals() {
        init();
        return literalObjects;
    }

    public void setLiterals(List<LiteralObject> literals) {
        this.literalObjects = literals;
    }

    public String getDecomposedFormula(){
        return this.statement.getFormulaAsString();
    }

    public boolean addToLiterals(LiteralObject object){
        init();
        boolean foundOposite = false;
        for(LiteralObject l : this.literalObjects){
            if(l.getName().equals(object.getName())){
                if(l.isNegated() == object.isNegated()){
                    return false;
                }
                else{
                    foundOposite = true;
                    break;
                }
            }
        }
        literalObjects.add(object);
        return foundOposite;
    }

    public boolean checkIfClosed(){
        return this.isClosed;
    }

    public boolean checkIfClosed(boolean noCheat){
        List<LiteralObject> l = new ArrayList<LiteralObject>();
        List<LiteralObject> ln = new ArrayList<LiteralObject>();

        for(LiteralObject l2 : this.literalObjects){
            if(l2.isNegated())
                ln.add(l2);
            else
                l.add(l2);
        }

        this.isClosed = false;

        for(LiteralObject l2 : l){
            for(LiteralObject l3 : ln){
                if(l2.getName().equals(l3.getName())){
                    this.isClosed = true;
                    return true;
                }
            }
        }

        return false;

    }

    public String toString(){
        return String.format("%s", this.nr);
    }

    public boolean isClosed() {
        return isClosed;
    }

    public boolean isRoot() {
        return isRoot;
    }

    public void setRoot(boolean isRoot) {
        this.isRoot = isRoot;
    }

}
