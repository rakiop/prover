package model.prover;

public enum Operator {
    Literal, AND, OR, NOT, IMP, EQ, F, G, LeftBrace, RightBrace, Unknown;
}
