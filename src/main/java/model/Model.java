package model;

import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.graph.DirectedOrderedSparseMultigraph;
import model.parser.FormulaNode;
import model.parser.Parser;
import model.prover.Prover;
import model.prover.ProverNode;

/**
 * Created by rakiop on 26.05.14.
 */
public class Model {

    private Parser parser;
    private Prover prover;
    private String formula;
    private FormulaNode root;
    private FormulaNode negateRoot;

    private DelegateTree<FormulaNode,String> formulaTree;
    private DelegateTree<FormulaNode,String> negateTree;
    private DelegateTree<ProverNode,String> proverTree;

    public ProverNode stepRoot;
    private DelegateTree<ProverNode,String> stepGraph;

    private boolean isStepMode = false;
    private int step = 1;
    private int currentStep = 0;
    private int stepCount = 0;
    private boolean isCheatMode = true;

    public Model(){
        parser = new Parser();
        prover = new Prover();
    }

    public String proveFormula(String formula){
        this.formula = formula;
        if (!formula.isEmpty()) {
            Long t = System.currentTimeMillis();
            try{
                root = parser.parse(formula);
                negateRoot = FormulaNode.copy(root);
            }catch (Exception e){
                return e.getMessage();
            }

            root.setRoot(true);
            try{
                ProverNode.step = 0;
                prover.prove(negateRoot, isCheatMode);
            }
            catch (Exception e){}
            Long t2 = System.currentTimeMillis();
            System.out.print("ms: ");
            System.out.println(t2-t);
            System.out.print("nodes: ");
            System.out.println(ProverNode.no);
            currentStep = 0;
            formulaTree.addVertex(root);
            createTree(root, formulaTree);

            negateRoot = prover.getNegatedFormulaRoot();
            negateRoot.setRoot(true);
            negateTree.addVertex(negateRoot);
            createTree(negateRoot, negateTree);

            ProverNode proverNode = prover.getProverRoot();
            proverNode.setRoot(true);
            proverTree.addVertex(proverNode);
            createTree(prover.getProverRoot(), proverTree);

            return prover.isOutput() ? "Formuła poprawna" : "Formuła niepoprawna";

        }
        else{
            return "Wprowadź formułę";
        }
    }

    public void clear(){
        formulaTree = new DelegateTree<FormulaNode, String>();
        negateTree = new DelegateTree<FormulaNode, String>();
        proverTree = new DelegateTree<ProverNode, String>();
        parser = new Parser();
        prover = new Prover();
        root = null;
        negateRoot = null;
        System.gc();
    }

    private void createTree(FormulaNode r,DelegateTree<FormulaNode,String> graph){
        if(r.getLeft() != null){
            graph.addChild(""+r.getNr()+r.getLeft().getNr(), r, r.getLeft());
            createTree(r.getLeft(),graph);
        }

        if(r.getRight() != null){
            graph.addChild(""+r.getNr()+r.getRight().getNr(),r, r.getRight());
            createTree(r.getRight(),graph);
        }
    }

    private void createTree(ProverNode r,DelegateTree<ProverNode,String> graph){
        if(r.getLeft() != null){
            graph.addChild(""+r.getNr()+r.getLeft().getNr(), r, r.getLeft());
            createTree(r.getLeft(),graph);
        }

        if(r.getRight() != null){
            graph.addChild(""+r.getNr()+r.getRight().getNr(),r, r.getRight());
            createTree(r.getRight(),graph);
        }
    }

    public DelegateTree<FormulaNode, String> getTree(){
        return formulaTree;
    }

    public DelegateTree<ProverNode, String> getProverTree(){
        return proverTree;
    }

    public DelegateTree<ProverNode, String> getNextStep(){
        stepGraph = new DelegateTree<ProverNode,String>(new DirectedOrderedSparseMultigraph<ProverNode,String>());

        stepRoot = prover.getProverRoot();
        stepRoot.setRoot(true);
        stepGraph.addVertex(stepRoot);

        this.currentStep = this.currentStep + this.step;
        this.stepCount = 1;
        createStepTree(stepRoot, stepGraph);
        return this.stepGraph;
    }

    public void createStepTree(ProverNode r,DelegateTree<ProverNode,String> graph){
        if(stepCount >= this.currentStep){
            return;
        }
        ++stepCount;

        if(r.getLeft() != null){
            graph.addChild(""+r.getNr()+r.getLeft().getNr(), r, r.getLeft());
            createStepTree(r.getLeft(),graph);
        }

        if(r.getRight() != null){
            graph.addChild(""+r.getNr()+r.getRight().getNr(),r, r.getRight());
            createStepTree(r.getRight(),graph);
        }
    }

    public DelegateTree<FormulaNode, String> getNegateTree(){
        return negateTree;
    }

    public void setCheatMode(boolean v){
        this.isCheatMode = v;
    }

    public void setStepMode(boolean v){
        this.isStepMode = v;
    }

    public boolean isStepMode(){
        return this.isStepMode;
    }

    public void setStep(int v){
        this.step = v;
    }
}
