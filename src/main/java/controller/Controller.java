package controller;

import model.Model;
import model.prover.Operator;
import model.prover.OperatorManagement;
import view.View;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * Created by rakiop on 26.05.14.
 */
public class Controller {
    View view;
    Model model;

    public Controller(View view, Model model){
        this.view = view;
        this.model = model;
    }

    public void startLogic(){
        // Przycisk wyjścia
        view.getExitButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        view.getSettingsView().getNegInput().setText(OperatorManagement.getInstance().getSymbol(Operator.NOT));
        view.getSettingsView().getAndInput().setText(OperatorManagement.getInstance().getSymbol(Operator.AND));
        view.getSettingsView().getOrInput().setText(OperatorManagement.getInstance().getSymbol(Operator.OR));
        view.getSettingsView().getImpInput().setText(OperatorManagement.getInstance().getSymbol(Operator.IMP));
        view.getSettingsView().getEqInput().setText(OperatorManagement.getInstance().getSymbol(Operator.EQ));
        view.getSettingsView().getgInput().setText(OperatorManagement.getInstance().getSymbol(Operator.G));
        view.getSettingsView().getfInput().setText(OperatorManagement.getInstance().getSymbol(Operator.F));

        view.getSettingsView().getNegPriority().setText(OperatorManagement.getInstance().getPriority(Operator.NOT).toString());
        view.getSettingsView().getAndPriority().setText(OperatorManagement.getInstance().getPriority(Operator.AND).toString());
        view.getSettingsView().getOrPriority().setText(OperatorManagement.getInstance().getPriority(Operator.OR).toString());
        view.getSettingsView().getImpPriority().setText(OperatorManagement.getInstance().getPriority(Operator.IMP).toString());
        view.getSettingsView().getEqPriority().setText(OperatorManagement.getInstance().getPriority(Operator.EQ).toString());
        view.getSettingsView().getgPriority().setText(OperatorManagement.getInstance().getPriority(Operator.G).toString());
        view.getSettingsView().getfPriority().setText(OperatorManagement.getInstance().getPriority(Operator.F).toString());

        view.getSettingsView().getSaveButton().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {

                OperatorManagement.getInstance().setSymbol(Operator.NOT, view.getSettingsView().getNegInput().getText());
                OperatorManagement.getInstance().setSymbol(Operator.AND, view.getSettingsView().getAndInput().getText());
                OperatorManagement.getInstance().setSymbol(Operator.OR, view.getSettingsView().getOrInput().getText());
                OperatorManagement.getInstance().setSymbol(Operator.IMP, view.getSettingsView().getImpInput().getText());
                OperatorManagement.getInstance().setSymbol(Operator.EQ, view.getSettingsView().getEqInput().getText());
                OperatorManagement.getInstance().setSymbol(Operator.F, view.getSettingsView().getfInput().getText());
                OperatorManagement.getInstance().setSymbol(Operator.G, view.getSettingsView().getgInput().getText());

                OperatorManagement.getInstance().setPriority(Operator.NOT, Integer.parseInt(view.getSettingsView().getNegPriority().getText()));
                OperatorManagement.getInstance().setPriority(Operator.AND, Integer.parseInt(view.getSettingsView().getAndPriority().getText()));
                OperatorManagement.getInstance().setPriority(Operator.OR, Integer.parseInt(view.getSettingsView().getOrPriority().getText()));
                OperatorManagement.getInstance().setPriority(Operator.IMP, Integer.parseInt(view.getSettingsView().getImpPriority().getText()));
                OperatorManagement.getInstance().setPriority(Operator.EQ, Integer.parseInt(view.getSettingsView().getEqPriority().getText()));
                OperatorManagement.getInstance().setPriority(Operator.F, Integer.parseInt(view.getSettingsView().getfPriority().getText()));
                OperatorManagement.getInstance().setPriority(Operator.G, Integer.parseInt(view.getSettingsView().getgPriority().getText()));

                model.setStepMode(view.getSettingsView().getStepMode().isSelected());
                model.setStep(Integer.parseInt(view.getSettingsView().getStepSize().getText()));

                view.getSemanticTreeView().getStepButton().setVisible(model.isStepMode());

                model.setCheatMode(view.getSettingsView().getCheatMode().isSelected());

            }
        });

        view.getSettingsView().getSaveFileButton().addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {
                File fileName = null;

                JFileChooser chooser = new JFileChooser();
                chooser.setCurrentDirectory( new File( "./") );
                int actionDialog = chooser.showSaveDialog(view.getSettingsView());
                if ( actionDialog == JFileChooser.APPROVE_OPTION )
                {
                    fileName = new File( chooser.getSelectedFile( ) + ".txt" );
                    if(fileName == null)
                        return;
                    if(fileName.exists())
                    {
                        actionDialog = JOptionPane.showConfirmDialog(view.getSettingsView(),
                                "Replace existing file?");
                        // may need to check for cancel option as well
                        if (actionDialog == JOptionPane.NO_OPTION)
                            return;
                    }
                }
                BufferedWriter bw = null;
                try {
                    FileWriter fw = new FileWriter(fileName);
                    bw = new BufferedWriter(fw);
                    bw.write(view.getSettingsView().getNegInput().getText());
                    bw.newLine();
                    bw.write(view.getSettingsView().getAndInput().getText());
                    bw.newLine();
                    bw.write(view.getSettingsView().getOrInput().getText());
                    bw.newLine();
                    bw.write(view.getSettingsView().getImpInput().getText());
                    bw.newLine();
                    bw.write(view.getSettingsView().getEqInput().getText());
                    bw.newLine();
                    bw.write(view.getSettingsView().getgInput().getText());
                    bw.newLine();
                    bw.write(view.getSettingsView().getfInput().getText());
                    bw.newLine();

                    bw.write(view.getSettingsView().getNegPriority().getText());
                    bw.newLine();
                    bw.write(view.getSettingsView().getAndPriority().getText());
                    bw.newLine();
                    bw.write(view.getSettingsView().getOrPriority().getText());
                    bw.newLine();
                    bw.write(view.getSettingsView().getImpPriority().getText());
                    bw.newLine();
                    bw.write(view.getSettingsView().getEqPriority().getText());
                    bw.newLine();
                    bw.write(view.getSettingsView().getgPriority().getText());
                    bw.newLine();
                    bw.write(view.getSettingsView().getfPriority().getText());
                    bw.newLine();

                    if(view.getSettingsView().getStepMode().isSelected())
                        bw.write("true");
                    else
                        bw.write("false");
                    bw.newLine();
                    bw.write(view.getSettingsView().getStepSize().getText());
                    bw.newLine();
                    if(view.getSettingsView().getNegMode().isSelected())
                        bw.write("true");
                    else
                        bw.write("false");
                    bw.newLine();
                    if(view.getSettingsView().getCheatMode().isSelected())
                        bw.write("true");
                    else
                        bw.write("false");
                    bw.newLine();

                } catch (IOException ex) {
                    System.out.println("Nie można zapisac ustawien");
                } finally {
                    try {
                        bw.close();
                    } catch(IOException ex) {
                        System.out.println("Nie można zamknac pliku");
                    }
                }

            }
        });

        view.getSettingsView().getLoadFileButton().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();

                File f = fileChooser.getSelectedFile();
                BufferedReader br = null;
                try {
                    br = new BufferedReader(new FileReader(f));
                    view.getSettingsView().getNegInput().setText(br.readLine());
                    view.getSettingsView().getAndInput().setText(br.readLine());
                    view.getSettingsView().getOrInput().setText(br.readLine());
                    view.getSettingsView().getImpInput().setText(br.readLine());
                    view.getSettingsView().getEqInput().setText(br.readLine());
                    view.getSettingsView().getgInput().setText(br.readLine());
                    view.getSettingsView().getfInput().setText(br.readLine());

                    view.getSettingsView().getNegPriority().setText(br.readLine());
                    view.getSettingsView().getAndPriority().setText(br.readLine());
                    view.getSettingsView().getOrPriority().setText(br.readLine());
                    view.getSettingsView().getImpPriority().setText(br.readLine());
                    view.getSettingsView().getEqPriority().setText(br.readLine());
                    view.getSettingsView().getgPriority().setText(br.readLine());
                    view.getSettingsView().getfPriority().setText(br.readLine());
                    if(br.readLine().equals("true"))
                        view.getSettingsView().getStepMode().setSelected(true);
                    else
                        view.getSettingsView().getStepMode().setSelected(false);
                    view.getSettingsView().getStepSize().setText(br.readLine());
                    if(br.readLine().equals("true"))
                        view.getSettingsView().getNegMode().setSelected(true);
                    else
                        view.getSettingsView().getNegMode().setSelected(false);
                    if(br.readLine().equals("true"))
                        view.getSettingsView().getCheatMode().setSelected(true);
                    else
                        view.getSettingsView().getCheatMode().setSelected(false);

                } catch(IOException ex) {
                    System.out.println("Nie można odczytac ustawien");
                } finally {
                    try {
                        br.close();
                    } catch(IOException ex) {
                        System.out.println("Nie można zamknąć pliku");
                    }
                }
            }
        });


        //Akcja analizy formuły
        view.getMainView().getProverAction().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.getFormulaTreeView().removeAll();
                view.getSemanticTreeView().removeAll();
                view.getNegateTreeView().removeAll();
                model.clear();

                view.getSemanticTreeView().getStepButton().setVisible(view.getSettingsView().getStepMode().isSelected());

                String formula  = view.getMainView().getFormula().getText();
                String wynik = model.proveFormula(formula);
                view.getMainView().getFormulaResultLabel().setText(wynik);

                if(model.getTree().getVertexCount() > 0){
                    view.getFormulaTreeView().setTree(model.getTree());
                    view.getFormulaTreeView().showTree();

                    view.getNegateTreeView().setTree(model.getNegateTree());
                    view.getNegateTreeView().showTree();

                    if(model.isStepMode()){
                        view.getSemanticTreeView().setTree(model.getNextStep());
                    }else{
                        view.getSemanticTreeView().setTree(model.getProverTree());
                    }
                    view.getSemanticTreeView().showTree();

                }
            }
        });

        view.getSemanticTreeView().getStepButton().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {

                if (model.isStepMode() == false) {
                    return;
                }

                view.getSemanticTreeView().removeAll();
                view.getSemanticTreeView().setTree(model.getNextStep());
                view.getSemanticTreeView().showTree();

            }
        });
    }
}
